#include <stdint.h>
#include <errno.h>
#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

// Version should be set via the Makefile

#ifndef __VERSION
#warning "__VERSION was not defined!"
#define __VERSION UNKNOWN
#endif

#define STR2(x) #x
#define STR(x) STR2(x)

// Parameters which will be read from the input file
char output_filename[80];
char result_filename[80];
char progress_filename[80];
char progress_bak_filename[80];
uint32_t print_freq;
uint32_t output_format;
#define FULL 0
#define COMPRESSED 1
#define NONE 2

// Bounds for each of the free variables
uint32_t e_min,e_max,
         i_min,i_max,
         k_min,k_max,
         l_min,l_max,
         m_min,m_max,
         o_min,o_max,
         p_min,p_max,
         r_min,r_max,
         u_min,u_max,
         v_min,v_max,
         x_min,x_max,
         y_min,y_max,
         z_min,z_max;

double elapsedTime(struct timeval start)
{
  struct timeval end;
  gettimeofday(&end, NULL);
  return (double)(end.tv_sec - start.tv_sec) + (double)(end.tv_usec - start.tv_usec) / 1000000.0;
}

bool entryIsUnused(int32_t n, uint32_t *num, uint32_t end, uint32_t *N)
{
  uint32_t i;
  for (i = 0; i <= end; i++)
  {
    if (num[i] == n)
    {
      *N=i;
      return true;
    }
  }
  return false;
}

void set_value(char *key, char *value)
{
  // Set correct variable matching key string
  if (strcmp(key, "output") == 0)
  {
    sscanf(value, "%s",output_filename); return;
  }
  if (strcmp(key, "format") == 0)
  {
    char format[80];
    sscanf(value, "%s",format); 
    if (strcmp(format, "full") == 0) output_format = FULL;
    if (strcmp(format, "compressed") == 0) output_format = COMPRESSED;
    if (strcmp(format, "none") == 0) output_format = NONE;
    return;
  }
  if (strcmp(key, "result") == 0)
  {
    sscanf(value, "%s",result_filename); return;
  }
  if (strcmp(key, "progress") == 0)
  {
    sscanf(value, "%s",progress_filename);
    strcpy(progress_bak_filename, progress_filename);
    strcat(progress_bak_filename, ".bak");
    return;
  }
  if (strcmp(key, "print") == 0)
  {
    sscanf(value, "%u",&print_freq); return;
  }
  if (strcmp(key, "e_max") == 0)
  {
    sscanf(value, "%u",&e_max); return;
  }
  if (strcmp(key, "e_min") == 0)
  {
    sscanf(value, "%u",&e_min); return;
  }
  if (strcmp(key, "i_max") == 0)
  {
    sscanf(value, "%u",&i_max); return;
  }
  if (strcmp(key, "i_min") == 0)
  {
    sscanf(value, "%u",&i_min); return;
  }
  if (strcmp(key, "k_max") == 0)
  {
    sscanf(value, "%u",&k_max); return;
  }
  if (strcmp(key, "k_min") == 0)
  {
    sscanf(value, "%u",&k_min); return;
  }
  if (strcmp(key, "l_max") == 0)
  {
    sscanf(value, "%u",&l_max); return;
  }
  if (strcmp(key, "l_min") == 0)
  {
    sscanf(value, "%u",&l_min); return;
  }
  if (strcmp(key, "m_max") == 0)
  {
    sscanf(value, "%u",&m_max); return;
  }
  if (strcmp(key, "m_min") == 0)
  {
    sscanf(value, "%u",&m_min); return;
  }
  if (strcmp(key, "o_max") == 0)
  {
    sscanf(value, "%u",&o_max); return;
  }
  if (strcmp(key, "o_min") == 0)
  {
    sscanf(value, "%u",&o_min); return;
  }
  if (strcmp(key, "p_max") == 0)
  {
    sscanf(value, "%u",&p_max); return;
  }
  if (strcmp(key, "p_min") == 0)
  {
    sscanf(value, "%u",&p_min); return;
  }
  if (strcmp(key, "r_max") == 0)
  {
    sscanf(value, "%u",&r_max); return;
  }
  if (strcmp(key, "r_min") == 0)
  {
    sscanf(value, "%u",&r_min); return;
  }
  if (strcmp(key, "u_max") == 0)
  {
    sscanf(value, "%u",&u_max); return;
  }
  if (strcmp(key, "u_min") == 0)
  {
    sscanf(value, "%u",&u_min); return;
  }
  if (strcmp(key, "v_max") == 0)
  {
    sscanf(value, "%u",&v_max); return;
  }
  if (strcmp(key, "v_min") == 0)
  {
    sscanf(value, "%u",&v_min); return;
  }
  if (strcmp(key, "x_max") == 0)
  {
    sscanf(value, "%u",&x_max); return;
  }
  if (strcmp(key, "x_min") == 0)
  {
    sscanf(value, "%u",&x_min); return;
  }
  if (strcmp(key, "y_max") == 0)
  {
    sscanf(value, "%u",&y_max); return;
  }
  if (strcmp(key, "y_min") == 0)
  {
    sscanf(value, "%u",&y_min); return;
  }
  if (strcmp(key, "z_max") == 0)
  {
    sscanf(value, "%u",&z_max); return;
  }
  if (strcmp(key, "z_min") == 0)
  {
    sscanf(value, "%u",&z_min); return;
  }
}

void read_input(char *filename)
{
  FILE *inp = fopen(filename, "r");
  if (!inp)
  {
    printf("Error opening file: %s\n",filename);
    exit(1);
  }

  // Use the defaults
  strcpy(output_filename, "squares.out");
  strcpy(result_filename, "results.txt");
  strcpy(progress_filename, "progress.txt");
  strcpy(progress_filename, "progress.txt.bak");
  print_freq = 10000;
  output_format = FULL;
  e_min=1; e_max = 36;
  i_min=1; i_max = 36;
  k_min=1; k_max = 36;
  l_min=1; l_max = 36;
  m_min=1; m_max = 36;
  o_min=1; o_max = 36;
  p_min=1; p_max = 36;
  r_min=1; r_max = 36;
  u_min=1; u_max = 36;
  v_min=1; v_max = 36;
  x_min=1; x_max = 36;
  y_min=1; y_max = 36;
  z_min=1; z_max = 36;

  // Read in key/value pairs
  char key[80], value[80];
  while (fscanf(inp, "%s %s", key, value) == 2)
  { 
    set_value(key, value);
  }

  fclose(inp);

}

void print_square(int32_t sq[36], FILE *outf)
{
  uint64_t o[4];
  switch(output_format){
    case FULL:
      fprintf(outf, "%d %d %d %d %d %d\n",sq[0],sq[1],sq[2],sq[3],sq[4],sq[5]);
      fprintf(outf, "%d %d %d %d %d %d\n",sq[6],sq[7],sq[8],sq[9],sq[10],sq[11]);
      fprintf(outf, "%d %d %d %d %d %d\n",sq[12],sq[13],sq[14],sq[15],sq[16],sq[17]);
      fprintf(outf, "%d %d %d %d %d %d\n",sq[18],sq[19],sq[20],sq[21],sq[22],sq[23]);
      fprintf(outf, "%d %d %d %d %d %d\n",sq[24],sq[25],sq[26],sq[27],sq[28],sq[29]);
      fprintf(outf, "%d %d %d %d %d %d\n",sq[30],sq[31],sq[32],sq[33],sq[34],sq[35]);
      break;
    case COMPRESSED:
      o[0] = (((uint64_t)sq[0] & 0x3f) << 0) | (((uint64_t)sq[1] & 0x3f) << 6) |
             (((uint64_t)sq[2] & 0x3f) << 12) | (((uint64_t)sq[3] & 0x3f) << 18) |
             (((uint64_t)sq[4] & 0x3f) << 24) | (((uint64_t)sq[5] & 0x3f) << 30) |
             (((uint64_t)sq[6] & 0x3f) << 36) | (((uint64_t)sq[7] & 0x3f) << 42) |
             (((uint64_t)sq[8] & 0x3f) << 48);

      o[1] = (((uint64_t)sq[9] & 0x3f) << 0) | (((uint64_t)sq[10] & 0x3f) << 6) |
             (((uint64_t)sq[11] & 0x3f) << 12) | (((uint64_t)sq[12] & 0x3f) << 18) |
             (((uint64_t)sq[13] & 0x3f) << 24) | (((uint64_t)sq[14] & 0x3f) << 30) |
             (((uint64_t)sq[15] & 0x3f) << 36) | (((uint64_t)sq[16] & 0x3f) << 42) |
             (((uint64_t)sq[17] & 0x3f) << 48);

      o[2] = (((uint64_t)sq[18] & 0x3f) << 0) | (((uint64_t)sq[19] & 0x3f) << 6) |
             (((uint64_t)sq[20] & 0x3f) << 12) | (((uint64_t)sq[21] & 0x3f) << 18) |
             (((uint64_t)sq[22] & 0x3f) << 24) | (((uint64_t)sq[23] & 0x3f) << 30) |
             (((uint64_t)sq[24] & 0x3f) << 36) | (((uint64_t)sq[25] & 0x3f) << 42) |
             (((uint64_t)sq[26] & 0x3f) << 48);

      o[3] = (((uint64_t)sq[27] & 0x3f) << 0) | (((uint64_t)sq[28] & 0x3f) << 6) |
             (((uint64_t)sq[29] & 0x3f) << 12) | (((uint64_t)sq[30] & 0x3f) << 18) |
             (((uint64_t)sq[31] & 0x3f) << 24) | (((uint64_t)sq[32] & 0x3f) << 30) |
             (((uint64_t)sq[33] & 0x3f) << 36) | (((uint64_t)sq[34] & 0x3f) << 42) |
             (((uint64_t)sq[35] & 0x3f) << 48);

      fprintf(outf, "%llu %llu %llu %llu\n",o[0], o[1], o[2], o[3]);
      break;
    case NONE:
      // No output
      break;
    default:
      break;
  }
}

void write_current_progress(uint32_t o, uint32_t m, uint32_t e, uint32_t p, uint32_t r, uint32_t y, uint32_t i)
{
  // Make a copy of the current progress file (if it exists)
  rename(progress_filename, progress_bak_filename); 

  // Write a new progress file

  FILE *outf = fopen(progress_filename, "w");
  if (!outf)
  {
    printf("Error opening file: %s\n",progress_filename);
    exit(1);
  }

  fprintf(outf,"Done: o=%u,m=%u,e=%u,p=%u,r=%u,y=%u,i=%u\n",o,m,e,p,r,y,i);

  if (fclose(outf))
  {
    printf("Error closing file: %s\n",progress_filename);
    exit(1);
  }

  // Delete the backup file

  unlink(progress_bak_filename);

}

int main(int argc, char** argv)
{
  printf("Program to generate semi-pandiagonal rank-6 magic squares\n");
  printf("By Saleem Al-Ashhab and Iain Bethune\n");
  printf("Version " STR(__VERSION) "\n");
  printf("---------------------------------------------------------\n");

  if (argc != 2)
  {
    printf("Usage: sp_squares <input_file>\n");
    exit(1);
  }

  struct timeval start, start_square;
  gettimeofday(&start, NULL);
 
  read_input(argv[1]);

  printf("Parameter set:\n");
  if (e_min != 1 || e_max != 36) printf("e = (%u .. %u)\n",e_min,e_max);
  if (i_min != 1 || i_max != 36) printf("i = (%u .. %u)\n",i_min,i_max);
  if (k_min != 1 || k_max != 36) printf("k = (%u .. %u)\n",k_min,k_max);
  if (l_min != 1 || l_max != 36) printf("l = (%u .. %u)\n",l_min,l_max);
  if (m_min != 1 || m_max != 36) printf("m = (%u .. %u)\n",m_min,m_max);
  if (o_min != 1 || o_max != 36) printf("o = (%u .. %u)\n",o_min,o_max);
  if (p_min != 1 || p_max != 36) printf("p = (%u .. %u)\n",p_min,p_max);
  if (r_min != 1 || r_max != 36) printf("r = (%u .. %u)\n",r_min,r_max);
  if (u_min != 1 || u_max != 36) printf("u = (%u .. %u)\n",u_min,u_max);
  if (v_min != 1 || v_max != 36) printf("v = (%u .. %u)\n",v_min,v_max);
  if (x_min != 1 || x_max != 36) printf("x = (%u .. %u)\n",x_min,x_max);
  if (y_min != 1 || y_max != 36) printf("y = (%u .. %u)\n",y_min,y_max);
  if (z_min != 1 || z_max != 36) printf("z = (%u .. %u)\n",z_min,z_max);
  printf("---------------------------------------------------------\n");

  FILE *outf;
  if (output_format != NONE)
  {
    outf = fopen(output_filename, "w");
    if (!outf)
    {
      printf("Error opening file: %s\n",output_filename);
      exit(1);
    }
  }

  uint32_t last_valid = 0;
  uint32_t a,A,B,c,d,D,e,E,f,F,h,H,i,J,k,K,l,L,m,M,n,N,o,p,q,Q,r,R,u,v,x,y,z,z2,e2;

  int32_t sq[36];
  uint32_t num[36];
  const uint32_t sum = 37;
  uint64_t valid_squares = 0;
  uint64_t total_squares = 0;

  // Initialise list of numbers
  for (i = 0; i < 36; i++)
  {
    num[i] = i+1;
    sq[i] = 0;
  }
  
  printf("Cores checked | Time taken | SPs Found | Total SPs\n");
  gettimeofday(&start_square, NULL);
  for (u = 0; u <= 35; u++)
  {
    if (num[u] < u_min || num[u] > u_max) continue;
    sq[14] = num[u]; 
    num[u] = num[35];
    num[35] = sq[14];

    for (v = 0; v <= 34; v++)
    {
      if (num[v] < v_min || num[v] > v_max) continue;
      sq[15] = num[v];
      num[v] = num[34];
      num[34] = sq[15];

      for (z = 0; z <= 33; z++)
      {
        if (num[z] < z_min || num[z] > z_max) continue;
        sq[20] = num[z];
        num[z] = num[33];
        num[33] = sq[20];

        sq[21] = 2*sum-sq[20]-sq[15]-sq[14];
        if (sq[21] >= 1 && sq[21] <= 36 && entryIsUnused(sq[21], num, 32, &z2))
        {
          num[z2] = num[32];
          num[32] = sq[21];
  
          for (o = 0; o <= 31; o++)
          {
            if (num[o] < o_min || num[o] > o_max) continue;
            sq[25] = num[o];
            num[o] = num[31];
            num[31] = sq[25];
  
            for (m = 0; m <= 30; m++)
            {
              if (num[m] < m_min || num[m] > m_max || num[m] > sq[25]) continue;
              sq[10] = num[m];
              num[m] = num[30];
              num[30] = sq[10];
  
              for (e = 0; e <= 29; e++)
              {
                if (num[e] < e_min || num[e] > e_max) continue;
                sq[28] = num[e];
                num[e] = num[29];
                num[29] = sq[28];
  
                sq[7] = 2*sum-sq[10]-sq[25]-sq[28];
                if (sq[7] >= 1 && sq[7] <= 36 && entryIsUnused(sq[7], num, 28, &e2))
                {
                  num[e2] = num[28];
                  num[28] = sq[7];
    
                  for (p = 0; p <= 27; p++)
                  {
                    if (num[p] < p_min || num[p] > p_max) continue;
                    sq[19] = num[p];
                    num[p] = num[27];
                    num[27] = sq[19];
    
                    for (r = 0; r <= 26; r++)
                    {
                      if (num[r] < r_min || num[r] > r_max) continue;
                      sq[13] = num[r];
                      num[r] = num[26];
                      num[26] = sq[13];
    
                      for (y = 0; y <= 25; y++)
                      {
                        if (num[y] < y_min || num[y] > y_max) continue;
                        sq[22] = num[y];
                        num[y] = num[25];
                        num[25] = sq[22];
    
                        for (i = 0; i <= 24; i++)
                        {
                          if (num[i] < i_min || num[i] > i_max) continue;
                          sq[26] = num[i];
                          num[i] = num[24];
                          num[24] = sq[26];
    
                          for (k = 0; k <= 23; k++)
                          {
                            if (num[k] < k_min || num[k] > k_max) continue;
                            sq[8] = num[k];
                            num[k] = num[23];
                            num[23] = sq[8];
    
                            for (l = 0; l <= 22; l++)
                            {
                              if (num[l] < l_min || num[l] > l_max) continue;
                              sq[9] = num[l];
                              num[l] = num[22];
                              num[22] = sq[9];
    
                              for (x = 0; x <= 21; x++)
                              {
                                if (num[x] < x_min || num[x] > x_max) continue;
                                sq[27] = num[x];
                                num[x] = num[21];
                                num[21] = sq[27];
    
                                sq[16] = 4*sum-sq[9]-sq[8]-sq[13]-sq[19]-sq[22]-sq[26]-sq[27];
                                if (sq[16] >= 1 && sq[16] <= 36 && entryIsUnused(sq[16], num, 20, &J))
                                {
                                  num[J] = num[20];
                                  num[20] = sq[16];
      
                                  // We have generated a valid inner 4x4 core
                                  // Now go and check the outside
      
                                  for (f = 0; f <= 19; f++)
                                  {
                                    sq[4] = num[f];
                                    num[f] = num[19];
                                    num[19] = sq[4];
      
                                    sq[34]=3*sum-sq[4]-sq[10]-sq[16]-sq[22]-sq[28];
                                    if (sq[34] >= 1 && sq[34] <= 36 && entryIsUnused(sq[34], num, 18, &F))
                                    {
                                      num[F] = num[18];
                                      num[18] = sq[34];
        
                                      for (d = 0; d <= 17; d++)
                                      {
                                        sq[3] = num[d];
                                        num[d] = num[17];
                                        num[17] = sq[3];
        
                                        sq[33]=3*sum-sq[3]-sq[9]-sq[15]-sq[21]-sq[27];
                                        if (sq[33] >= 1 && sq[33] <= 36 && entryIsUnused(sq[33], num, 16, &R))
                                        {
                                          num[R] = num[16];
                                          num[16] = sq[33];
          
                                          for (c = 0; c <= 15; c++)
                                          {
                                            sq[2] = num[c];
                                            num[c] = num[15];
                                            num[15] = sq[2];
          
                                            sq[32]=3*sum-sq[2]-sq[8]-sq[14]-sq[20]-sq[26];
                                            if (sq[32] >= 1 && sq[32] <= 36 && entryIsUnused(sq[32], num, 14, &Q))
                                            {
                                              num[Q] = num[14];
                                              num[14] = sq[32];
            
                                              for (h = 0; h <= 13; h++)
                                              {
                                                sq[6] = num[h];
                                                num[h] = num[13];
                                                num[13] = sq[6];
            
                                                sq[11]=3*sum-sq[6]-sq[8]-sq[7]-sq[9]-sq[10];
                                                if (sq[11] >= 1 && sq[11] <= 36 && entryIsUnused(sq[11], num, 12, &H))
                                                  {
                                                  num[H] = num[12];
                                                  num[12] = sq[11];
              
                                                  for (q = 0; q <= 11; q++)
                                                  {
                                                    sq[18] = num[q];
                                                    num[q] = num[11];
                                                    num[11] = sq[18];
              
                                                    sq[23]=3*sum-sq[22]-sq[21]-sq[20]-sq[19]-sq[18];
                                                    if (sq[23] >= 1 && sq[23] <= 36 && entryIsUnused(sq[23], num, 10, &L))
                                                    {
                                                      num[L] = num[10];
                                                      num[10] = sq[23];
                
                                                      sq[12]=sq[3]-sq[2]+sq[9]+sq[10]+sq[19]+sq[25]+sq[18]-sum-2*sq[14]-sq[15]+sq[22]+sq[27]-sq[20];
                                                      if (sq[12] >= 1 && sq[12] <= 36 && entryIsUnused(sq[12], num, 9, &A))
                                                      {
                                                        num[A] = num[9];
                                                        num[9] = sq[12];
                  
                                                        sq[17]=3*sum-sq[12]-sq[13]-sq[14]-sq[15]-sq[16];
                                                        if (sq[17] >= 1 && sq[17] <= 36 && entryIsUnused(sq[17], num, 8, &K))
                                                        {
                                                          num[K] = num[8];
                                                          num[8] = sq[17];
                    
                                                          for (n = 0; n <= 7; n++)
                                                          {
                                                            sq[24] = num[n];
                                                            num[n] = num[7];
                                                            num[7] = sq[24];
                    
                                                            sq[29]=3*sum-sq[24]-sq[25]-sq[26]-sq[27]-sq[28];
                                                            if (sq[29] >= 1 && sq[29] <= 36 && entryIsUnused(sq[29], num, 6, &M))
                                                            {
                                                              num[M] = num[6];
                                                              num[6] = sq[29];
                      
                                                              for (a = 0; a <= 5; a++)
                                                              {
                                                                sq[0] = num[a];
                                                                num[a] = num[5];
                                                                num[5] = sq[0];
                      
                                                                sq[30]=3*sum-sq[0]-sq[6]-sq[12]-sq[18]-sq[24];
                                                                if (sq[30] >= 1 && sq[30] <= 36 && entryIsUnused(sq[30], num, 4, &B))
                                                                {
                                                                  num[B] = num[4];
                                                                  num[4] = sq[30];
                        
                                                                  sq[35]=sq[10]-sq[0]+sq[15]+sq[20]+sq[25]-sum;
                                                                  if (sq[35] >= 1 && sq[35] <= 36 && entryIsUnused(sq[35], num, 3, &N))
                                                                  {
                                                                    num[N] = num[3];
                                                                    num[3] = sq[35];
                         
                                                                    sq[31]=3*sum-sq[30]-sq[32]-sq[34]-sq[35]-sq[33];
                                                                    if (sq[31] >= 1 && sq[31] <= 36 && entryIsUnused(sq[31], num, 2, &E))
                                                                    {
                                                                      num[E] = num[2];
                                                                      num[2] = sq[31];
                            
                                                                      sq[1]=3*sum-sq[7]-sq[13]-sq[19]-sq[25]-sq[31];
                                                                      if (sq[1] >= 1 && sq[1] <= 36 && entryIsUnused(sq[1], num, 1, &D))
                                                                      {
                                                                        num[D] = num[1];
                                                                        num[1] = sq[1];
                              
                                                                        sq[5]=3*sum-sq[0]-sq[2]-sq[4]-sq[1]-sq[3];
                                                                        if (sq[5] == num[0])
                                                                        {
                                                                          // If we got this far, we have a valid square!
                              
                                                                          print_square(sq, outf);
                              
                                                                          valid_squares++;
                                                                        }
                                                                        num[1] = num[D];
                                                                        num[D] = sq[1];
                                                                      }
                                                                      num[2] = num[E];
                                                                      num[E] = sq[31];
                                                                    }
                                                                    num[3] = num[N];
                                                                    num[N] = sq[35];
                                                                  }
                                                                  num[4] = num[B];
                                                                  num[B] = sq[30];
                                                                }
                                                                num[5] = num[a];
                                                                num[a] = sq[0];
                                                              } // end of a loop
                                                              num[6] = num[M];
                                                              num[M] = sq[29];
                                                            }
                                                            num[7] = num[n];
                                                            num[n] = sq[24];
                                                          } // end of n loop
                                                          num[8] = num[K];
                                                          num[K] = sq[17];
                                                        }
                                                        num[9] = num[A];
                                                        num[A] = sq[12];
                                                      }
                                                      num[10] = num[L];
                                                      num[L] = sq[23];
                                                    }
                                                    num[11] = num[q];
                                                    num[q] = sq[18];
                                                  } // end of q loop
                                                  num[12] = num[H];
                                                  num[H] = sq[11];
                                                }
                                                num[13] = num[h];
                                                num[h] = sq[6];
                                              } // end of h loop
                                              num[14] = num[Q];
                                              num[Q] = sq[32];
                                            }
                                            num[15] = num[c];
                                            num[c] = sq[2];
                                          } // end of c loop
                                          num[16] = num[R];
                                          num[R] = sq[33];
                                        }
                                        num[17] = num[d];
                                        num[d] = sq[3];
                                      } // end of d loop
                                      num[18] = num[F];
                                      num[F] = sq[34];
                                    }
                                    num[19] = num[f];
                                    num[f] = sq[4];
                                  } // end of f loop
      
                                  // Finished checking the outside for this 4x4 core
                                  total_squares++;
                                  if (total_squares % print_freq == 0)
                                  {
                                    printf("%13llu   %9.3lfs   %9llu   %9llu\n",total_squares,elapsedTime(start_square), valid_squares - last_valid, valid_squares);
                                    last_valid = valid_squares;
                                    gettimeofday(&start_square, NULL);
                                  }
      
                                  num[20] = num[J];
                                  num[J] = sq[16];
                                }
                                num[21] = num[x];
                                num[x] = sq[27];
                              } // end of x loop
                              num[22] = num[l];
                              num[l] = sq[9];
                            } // end of l loop
                            num[23] = num[k];
                            num[k] = sq[8];
                          } // end of k loop
                          num[24] = num[i];
                          num[i] = sq[26];
                        write_current_progress(sq[25],sq[10],sq[28],sq[19],sq[13],sq[22],sq[26]);
                        } // end of i loop
                        num[25] = num[y];
                        num[y] = sq[22];
                        // Print out the current progress.
                        //write_current_progress(sq[25],sq[10],sq[28],sq[19],sq[13],sq[22],sq[26]);
                      } // end of y loop
                      num[26] = num[r];
                      num[r] = sq[13];
                    } // end of r loop
                    num[27] = num[p];
                    num[p] = sq[19];
                  } // end of p loop
                  num[28] = num[e2];
                  num[e2] = sq[7];
                }
                num[29] = num[e];
                num[e] = sq[28];
              } // end of e loop
              num[30] = num[m];
              num[m] = sq[10];
            } // end of m loop
            num[31] = num[o];
            num[o] = sq[25];
          } // end of o loop
          num[32] = num[z2];
          num[z2] = sq[21];
        }
        num[33] = num[z];
        num[z] = sq[20];
      } // end of z loop
      num[34] = num[v];
      num[v] = sq[15];
    } // end of v loop
    num[35] = num[u];
    num[u] = sq[14];
  } // end of u loop

  if (output_format != NONE)
  {
    fclose(outf);
  }

  printf("Finished!\n");
  printf("---------------------------------------------------------\n");
  printf("Total time        : %.3lfs\n",elapsedTime(start));
  printf("4x4 cores checked : %llu\n", total_squares);
  printf("SP Squares found  : %llu\n", valid_squares);

  outf = fopen(result_filename, "w");
  if (!outf)
  {
    printf("Error opening file: %s\n",result_filename);
    exit(1);
  }

  fprintf(outf, "Parameter set:\n");
  if (e_min != 1 || e_max != 36) fprintf(outf, "e = (%u .. %u)\n",e_min,e_max);
  if (i_min != 1 || i_max != 36) fprintf(outf, "i = (%u .. %u)\n",i_min,i_max);
  if (k_min != 1 || k_max != 36) fprintf(outf, "k = (%u .. %u)\n",k_min,k_max);
  if (l_min != 1 || l_max != 36) fprintf(outf, "l = (%u .. %u)\n",l_min,l_max);
  if (m_min != 1 || m_max != 36) fprintf(outf, "m = (%u .. %u)\n",m_min,m_max);
  if (o_min != 1 || o_max != 36) fprintf(outf, "o = (%u .. %u)\n",o_min,o_max);
  if (p_min != 1 || p_max != 36) fprintf(outf, "p = (%u .. %u)\n",p_min,p_max);
  if (r_min != 1 || r_max != 36) fprintf(outf, "r = (%u .. %u)\n",r_min,r_max);
  if (u_min != 1 || u_max != 36) fprintf(outf, "u = (%u .. %u)\n",u_min,u_max);
  if (v_min != 1 || v_max != 36) fprintf(outf, "v = (%u .. %u)\n",v_min,v_max);
  if (x_min != 1 || x_max != 36) fprintf(outf, "x = (%u .. %u)\n",x_min,x_max);
  if (y_min != 1 || y_max != 36) fprintf(outf, "y = (%u .. %u)\n",y_min,y_max);
  if (z_min != 1 || z_max != 36) fprintf(outf, "z = (%u .. %u)\n",z_min,z_max);
  fprintf(outf, "SP squares found: %llu\n", valid_squares);
  fprintf(outf, "Total time      : %.3lfs\n", elapsedTime(start));

  return 0;
}


