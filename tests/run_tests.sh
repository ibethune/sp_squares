#!/bin/bash

SP_SQUARES=../sp_squares
COMPRESS=../utils/read_squares

tests=`ls -1 sq_* | cut -c 4-`

echo "Test harness for SP Squares"
echo "---------------------------"

for test in $tests
do
  # Run the test input
  echo "Running test: $test"
  $SP_SQUARES test_${test}.inp > /dev/null
  if (( $? )); then
    echo "FAIL: Could not run test"
    exit
  fi

  # Check results against reference
  if [[ $test != "comp" ]]; then
    $COMPRESS squares.out | sort > new
  else
    sort squares.out > new
  fi

  $COMPRESS sq_$test | sort > old
  diff new old > /dev/null
  if (( $? )); then
    echo "FAIL: Test results did not match reference"
    exit
  fi 
done

echo "All tests completed successfully!"
