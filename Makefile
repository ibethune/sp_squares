# Mac
CC = clang-mp-3.6
CFLAGS = -O4 -Wall

# Ultra
#CC = icc
#CFLAGS = -fast -Wall

VER = $(shell git describe --long)

LD = $(CC)

OBJS = sp_squares.o
EXE = sp_squares

DFLAGS = -D__VERSION=$(VER)
LDFLAGS = $(CFLAGS)

$(EXE) : clean $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $(OBJS)

clean :
	rm -f *.o $(EXE)

.c.o:
	$(CC) $(CFLAGS) $(DFLAGS) -c $< -o $@
