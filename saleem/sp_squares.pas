{This program computes the semi pandiagonal magic squares}


program saleem;

label below3;

label below7;

label below9;

type  vector=array[1..36] of integer;

var

i1,i2,i3,i4,i5,i6,i14,i15,i16,i17,i18,i19,i20 : integer;
i7,i8,i9,i10,i11,i12,i13,s : integer;
count : real;
b:vector;
aut:text;

procedure square;

label below1;
label below2;
label below4;
label below5;
label below6;
label below8;
label below10;

begin

for i8:=1 to 36 do begin

  b[5]:=i8;

if b[5]=b[8] then goto below10;
if b[5]=b[9] then goto below10;
if b[5]=b[10] then goto below10;
if b[5]=b[11] then goto below10;
if b[5]=b[14] then goto below10;
if b[5]=b[15] then goto below10;
if b[5]=b[16] then goto below10;
if b[5]=b[17] then goto below10;
if b[5]=b[20] then goto below10;
if b[5]=b[21] then goto below10;
if b[5]=b[22] then goto below10;
if b[5]=b[23] then goto below10;
if b[5]=b[26] then goto below10;
if b[5]=b[27] then goto below10;
if b[5]=b[28] then goto below10;
if b[5]=b[29] then goto below10;

b[35]:=3*s-b[5]-b[11]-b[17]-b[23]-b[29];

if b[35]<1 then goto below10;
if b[35]>36 then goto below10;

if b[35]=b[5] then goto below10;
if b[35]=b[8] then goto below10;
if b[35]=b[9] then goto below10;
if b[35]=b[10] then goto below10;
if b[35]=b[11] then goto below10;
if b[35]=b[14] then goto below10;
if b[35]=b[15] then goto below10;
if b[35]=b[16] then goto below10;
if b[35]=b[17] then goto below10;
if b[35]=b[20] then goto below10;
if b[35]=b[21] then goto below10;
if b[35]=b[22] then goto below10;
if b[35]=b[23] then goto below10;
if b[35]=b[26] then goto below10;
if b[35]=b[27] then goto below10;
if b[35]=b[28] then goto below10;
if b[35]=b[29] then goto below10;

for i20:=1 to 36 do begin

  b[4]:=i20;

if b[4]=b[5] then goto below8;
if b[4]=b[8] then goto below8;
if b[4]=b[9] then goto below8;
if b[4]=b[10] then goto below8;
if b[4]=b[11] then goto below8;
if b[4]=b[14] then goto below8;
if b[4]=b[15] then goto below8;
if b[4]=b[16] then goto below8;
if b[4]=b[17] then goto below8;
if b[4]=b[20] then goto below8;
if b[4]=b[21] then goto below8;
if b[4]=b[22] then goto below8;
if b[4]=b[23] then goto below8;
if b[4]=b[26] then goto below8;
if b[4]=b[27] then goto below8;
if b[4]=b[28] then goto below8;
if b[4]=b[29] then goto below8;
if b[4]=b[35] then goto below8;

b[34]:=3*s-b[4]-b[10]-b[16]-b[22]-b[28];

if b[34]<1 then goto below8;
if b[34]>36 then goto below8;

if b[34]=b[4] then goto below8;
if b[34]=b[5] then goto below8;
if b[34]=b[8] then goto below8;
if b[34]=b[9] then goto below8;
if b[34]=b[10] then goto below8;
if b[34]=b[11] then goto below8;
if b[34]=b[14] then goto below8;
if b[34]=b[15] then goto below8;
if b[34]=b[16] then goto below8;
if b[34]=b[17] then goto below8;
if b[34]=b[20] then goto below8;
if b[34]=b[21] then goto below8;
if b[34]=b[22] then goto below8;
if b[34]=b[23] then goto below8;
if b[34]=b[26] then goto below8;
if b[34]=b[27] then goto below8;
if b[34]=b[28] then goto below8;
if b[34]=b[29] then goto below8;
if b[34]=b[35] then goto below8;

for i12:=1 to 36 do begin

  b[3]:=i12;

if b[3]=b[4] then goto below6;
if b[3]=b[5] then goto below6;
if b[3]=b[8] then goto below6;
if b[3]=b[9] then goto below6;
if b[3]=b[10] then goto below6;
if b[3]=b[11] then goto below6;
if b[3]=b[14] then goto below6;
if b[3]=b[15] then goto below6;
if b[3]=b[16] then goto below6;
if b[3]=b[17] then goto below6;
if b[3]=b[20] then goto below6;
if b[3]=b[21] then goto below6;
if b[3]=b[22] then goto below6;
if b[3]=b[23] then goto below6;
if b[3]=b[26] then goto below6;
if b[3]=b[27] then goto below6;
if b[3]=b[28] then goto below6;
if b[3]=b[29] then goto below6;
if b[3]=b[34] then goto below6;
if b[3]=b[35] then goto below6;

b[33]:=3*s-b[3]-b[9]-b[15]-b[21]-b[27];

if b[33]<1 then goto below6;
if b[33]>36 then goto below6;

if b[33]=b[3] then goto below6;
if b[33]=b[4] then goto below6;
if b[33]=b[5] then goto below6;
if b[33]=b[8] then goto below6;
if b[33]=b[9] then goto below6;
if b[33]=b[10] then goto below6;
if b[33]=b[11] then goto below6;
if b[33]=b[14] then goto below6;
if b[33]=b[15] then goto below6;
if b[33]=b[16] then goto below6;
if b[33]=b[17] then goto below6;
if b[33]=b[20] then goto below6;
if b[33]=b[21] then goto below6;
if b[33]=b[22] then goto below6;
if b[33]=b[23] then goto below6;
if b[33]=b[26] then goto below6;
if b[33]=b[27] then goto below6;
if b[33]=b[28] then goto below6;
if b[33]=b[29] then goto below6;
if b[33]=b[34] then goto below6;
if b[33]=b[35] then goto below6;

for i13:=1 to 36 do begin

  b[7]:=i13;

if b[7]=b[3] then goto below1;
if b[7]=b[4] then goto below1;
if b[7]=b[5] then goto below1;
if b[7]=b[8] then goto below1;
if b[7]=b[9] then goto below1;
if b[7]=b[10] then goto below1;
if b[7]=b[11] then goto below1;
if b[7]=b[14] then goto below1;
if b[7]=b[15] then goto below1;
if b[7]=b[16] then goto below1;
if b[7]=b[17] then goto below1;
if b[7]=b[20] then goto below1;
if b[7]=b[21] then goto below1;
if b[7]=b[22] then goto below1;
if b[7]=b[23] then goto below1;
if b[7]=b[26] then goto below1;
if b[7]=b[27] then goto below1;
if b[7]=b[28] then goto below1;
if b[7]=b[29] then goto below1;
if b[7]=b[33] then goto below1;
if b[7]=b[34] then goto below1;
if b[7]=b[35] then goto below1;

b[12]:=3*s-b[7]-b[9]-b[8]-b[10]-b[11];

if b[12]<1 then goto below1;
if b[12]>36 then goto below1;

if b[12]=b[3] then goto below1;
if b[12]=b[4] then goto below1;
if b[12]=b[5] then goto below1;
if b[12]=b[7] then goto below1;
if b[12]=b[8] then goto below1;
if b[12]=b[9] then goto below1;
if b[12]=b[10] then goto below1;
if b[12]=b[11] then goto below1;
if b[12]=b[14] then goto below1;
if b[12]=b[15] then goto below1;
if b[12]=b[16] then goto below1;
if b[12]=b[17] then goto below1;
if b[12]=b[20] then goto below1;
if b[12]=b[21] then goto below1;
if b[12]=b[22] then goto below1;
if b[12]=b[23] then goto below1;
if b[12]=b[26] then goto below1;
if b[12]=b[27] then goto below1;
if b[12]=b[28] then goto below1;
if b[12]=b[29] then goto below1;
if b[12]=b[33] then goto below1;
if b[12]=b[34] then goto below1;
if b[12]=b[35] then goto below1;

for i14:=1 to 36 do begin

  b[19]:=i14;

if b[19]=b[3] then goto below2;
if b[19]=b[4] then goto below2;
if b[19]=b[5] then goto below2;
if b[19]=b[7] then goto below2;
if b[19]=b[8] then goto below2;
if b[19]=b[9] then goto below2;
if b[19]=b[10] then goto below2;
if b[19]=b[11] then goto below2;
if b[19]=b[12] then goto below2;
if b[19]=b[14] then goto below2;
if b[19]=b[15] then goto below2;
if b[19]=b[16] then goto below2;
if b[19]=b[17] then goto below2;
if b[19]=b[20] then goto below2;
if b[19]=b[21] then goto below2;
if b[19]=b[22] then goto below2;
if b[19]=b[23] then goto below2;
if b[19]=b[26] then goto below2;
if b[19]=b[27] then goto below2;
if b[19]=b[28] then goto below2;
if b[19]=b[29] then goto below2;
if b[19]=b[33] then goto below2;
if b[19]=b[34] then goto below2;
if b[19]=b[35] then goto below2;

b[24]:=3*s-b[23]-b[22]-b[21]-b[20]-b[19];

if b[24]<1 then goto below2;
if b[24]>36 then goto below2;

if b[24]=b[3] then goto below2;
if b[24]=b[4] then goto below2;
if b[24]=b[5] then goto below2;
if b[24]=b[7] then goto below2;
if b[24]=b[8] then goto below2;
if b[24]=b[9] then goto below2;
if b[24]=b[10] then goto below2;
if b[24]=b[11] then goto below2;
if b[24]=b[12] then goto below2;
if b[24]=b[14] then goto below2;
if b[24]=b[15] then goto below2;
if b[24]=b[16] then goto below2;
if b[24]=b[17] then goto below2;
if b[24]=b[19] then goto below2;
if b[24]=b[20] then goto below2;
if b[24]=b[21] then goto below2;
if b[24]=b[22] then goto below2;
if b[24]=b[23] then goto below2;
if b[24]=b[26] then goto below2;
if b[24]=b[27] then goto below2;
if b[24]=b[28] then goto below2;
if b[24]=b[29] then goto below2;
if b[24]=b[33] then goto below2;
if b[24]=b[34] then goto below2;
if b[24]=b[35] then goto below2;

b[13]:=b[4]-b[3]+b[10]+b[11]+b[20]+b[26]+b[19]-s-2*b[15]-b[16]+b[23]+b[28]-b[21];

if b[13]<1 then goto below2;
if b[13]>36 then goto below2;

if b[13]=b[3] then goto below2;
if b[13]=b[4] then goto below2;
if b[13]=b[5] then goto below2;
if b[13]=b[7] then goto below2;
if b[13]=b[8] then goto below2;
if b[13]=b[9] then goto below2;
if b[13]=b[10] then goto below2;
if b[13]=b[11] then goto below2;
if b[13]=b[12] then goto below2;
if b[13]=b[14] then goto below2;
if b[13]=b[15] then goto below2;
if b[13]=b[16] then goto below2;
if b[13]=b[17] then goto below2;
if b[13]=b[19] then goto below2;
if b[13]=b[20] then goto below2;
if b[13]=b[21] then goto below2;
if b[13]=b[22] then goto below2;
if b[13]=b[23] then goto below2;
if b[13]=b[24] then goto below2;
if b[13]=b[26] then goto below2;
if b[13]=b[27] then goto below2;
if b[13]=b[28] then goto below2;
if b[13]=b[29] then goto below2;
if b[13]=b[33] then goto below2;
if b[13]=b[34] then goto below2;
if b[13]=b[35] then goto below2;
 
b[18]:=3*s-b[13]-b[14]-b[15]-b[16]-b[17];

if b[18]<1 then goto below2;
if b[18]>36 then goto below2;

if b[18]=b[3] then goto below2;
if b[18]=b[4] then goto below2;
if b[18]=b[5] then goto below2;
if b[18]=b[7] then goto below2;
if b[18]=b[8] then goto below2;
if b[18]=b[9] then goto below2;
if b[18]=b[10] then goto below2;
if b[18]=b[11] then goto below2;
if b[18]=b[12] then goto below2;
if b[18]=b[13] then goto below2;
if b[18]=b[14] then goto below2;
if b[18]=b[15] then goto below2;
if b[18]=b[16] then goto below2;
if b[18]=b[17] then goto below2;
if b[18]=b[19] then goto below2;
if b[18]=b[20] then goto below2;
if b[18]=b[21] then goto below2;
if b[18]=b[22] then goto below2;
if b[18]=b[23] then goto below2;
if b[18]=b[24] then goto below2;
if b[18]=b[26] then goto below2;
if b[18]=b[27] then goto below2;
if b[18]=b[28] then goto below2;
if b[18]=b[29] then goto below2;
if b[18]=b[33] then goto below2;
if b[18]=b[34] then goto below2;
if b[18]=b[35] then goto below2;

for i17:=1 to 36 do begin

  b[25]:=i17;

if b[25]=b[3] then goto below4;
if b[25]=b[4] then goto below4;
if b[25]=b[5] then goto below4;
if b[25]=b[7] then goto below4;
if b[25]=b[8] then goto below4;
if b[25]=b[9] then goto below4;
if b[25]=b[10] then goto below4;
if b[25]=b[11] then goto below4;
if b[25]=b[12] then goto below4;
if b[25]=b[13] then goto below4;
if b[25]=b[14] then goto below4;
if b[25]=b[15] then goto below4;
if b[25]=b[16] then goto below4;
if b[25]=b[17] then goto below4;
if b[25]=b[18] then goto below4;
if b[25]=b[19] then goto below4;
if b[25]=b[20] then goto below4;
if b[25]=b[21] then goto below4;
if b[25]=b[22] then goto below4;
if b[25]=b[23] then goto below4;
if b[25]=b[24] then goto below4;
if b[25]=b[26] then goto below4;
if b[25]=b[27] then goto below4;
if b[25]=b[28] then goto below4;
if b[25]=b[29] then goto below4;
if b[25]=b[33] then goto below4;
if b[25]=b[34] then goto below4;
if b[25]=b[35] then goto below4;

b[30]:=3*s-b[25]-b[26]-b[27]-b[28]-b[29];

if b[30]<1 then goto below4;
if b[30]>36 then goto below4;

if b[30]=b[3] then goto below4;
if b[30]=b[4] then goto below4;
if b[30]=b[5] then goto below4;
if b[30]=b[7] then goto below4;
if b[30]=b[8] then goto below4;
if b[30]=b[9] then goto below4;
if b[30]=b[10] then goto below4;
if b[30]=b[11] then goto below4;
if b[30]=b[12] then goto below4;
if b[30]=b[13] then goto below4;
if b[30]=b[14] then goto below4;
if b[30]=b[15] then goto below4;
if b[30]=b[16] then goto below4;
if b[30]=b[17] then goto below4;
if b[30]=b[18] then goto below4;
if b[30]=b[19] then goto below4;
if b[30]=b[20] then goto below4;
if b[30]=b[21] then goto below4;
if b[30]=b[22] then goto below4;
if b[30]=b[23] then goto below4;
if b[30]=b[24] then goto below4;
if b[30]=b[25] then goto below4;
if b[30]=b[26] then goto below4;
if b[30]=b[27] then goto below4;
if b[30]=b[28] then goto below4;
if b[30]=b[29] then goto below4;
if b[30]=b[33] then goto below4;
if b[30]=b[34] then goto below4;
if b[30]=b[35] then goto below4;

for i19:=1 to 36 do begin

  b[1]:=i19;

if b[1]=b[3] then goto below5;
if b[1]=b[4] then goto below5;
if b[1]=b[5] then goto below5;
if b[1]=b[7] then goto below5;
if b[1]=b[8] then goto below5;
if b[1]=b[9] then goto below5;
if b[1]=b[10] then goto below5;
if b[1]=b[11] then goto below5;
if b[1]=b[12] then goto below5;
if b[1]=b[13] then goto below5;
if b[1]=b[14] then goto below5;
if b[1]=b[15] then goto below5;
if b[1]=b[16] then goto below5;
if b[1]=b[17] then goto below5;
if b[1]=b[18] then goto below5;
if b[1]=b[19] then goto below5;
if b[1]=b[20] then goto below5;
if b[1]=b[21] then goto below5;
if b[1]=b[22] then goto below5;
if b[1]=b[23] then goto below5;
if b[1]=b[24] then goto below5;
if b[1]=b[25] then goto below5;
if b[1]=b[26] then goto below5;
if b[1]=b[27] then goto below5;
if b[1]=b[28] then goto below5;
if b[1]=b[29] then goto below5;
if b[1]=b[30] then goto below5;
if b[1]=b[33] then goto below5;
if b[1]=b[34] then goto below5;
if b[1]=b[35] then goto below5;

b[31]:=3*s-b[1]-b[7]-b[13]-b[19]-b[25];

if b[31]<1 then goto below5;
if b[31]>36 then goto below5;

if b[31]=b[1] then goto below5;
if b[31]=b[3] then goto below5;
if b[31]=b[4] then goto below5;
if b[31]=b[5] then goto below5;
if b[31]=b[7] then goto below5;
if b[31]=b[8] then goto below5;
if b[31]=b[9] then goto below5;
if b[31]=b[10] then goto below5;
if b[31]=b[11] then goto below5;
if b[31]=b[12] then goto below5;
if b[31]=b[13] then goto below5;
if b[31]=b[14] then goto below5;
if b[31]=b[15] then goto below5;
if b[31]=b[16] then goto below5;
if b[31]=b[17] then goto below5;
if b[31]=b[18] then goto below5;
if b[31]=b[19] then goto below5;
if b[31]=b[20] then goto below5;
if b[31]=b[21] then goto below5;
if b[31]=b[22] then goto below5;
if b[31]=b[23] then goto below5;
if b[31]=b[24] then goto below5;
if b[31]=b[25] then goto below5;
if b[31]=b[26] then goto below5;
if b[31]=b[27] then goto below5;
if b[31]=b[28] then goto below5;
if b[31]=b[29] then goto below5;
if b[31]=b[30] then goto below5;
if b[31]=b[33] then goto below5;
if b[31]=b[34] then goto below5;
if b[31]=b[35] then goto below5;

b[36]:=b[11]-b[1]+b[16]+b[21]+b[26]-s;

if b[36]<1 then goto below5;
if b[36]>36 then goto below5;

if b[36]=b[1] then goto below5;
if b[36]=b[3] then goto below5;
if b[36]=b[4] then goto below5;
if b[36]=b[5] then goto below5;
if b[36]=b[7] then goto below5;
if b[36]=b[8] then goto below5;
if b[36]=b[9] then goto below5;
if b[36]=b[10] then goto below5;
if b[36]=b[11] then goto below5;
if b[36]=b[12] then goto below5;
if b[36]=b[13] then goto below5;
if b[36]=b[14] then goto below5;
if b[36]=b[15] then goto below5;
if b[36]=b[16] then goto below5;
if b[36]=b[17] then goto below5;
if b[36]=b[18] then goto below5;
if b[36]=b[19] then goto below5;
if b[36]=b[20] then goto below5;
if b[36]=b[21] then goto below5;
if b[36]=b[22] then goto below5;
if b[36]=b[23] then goto below5;
if b[36]=b[24] then goto below5;
if b[36]=b[25] then goto below5;
if b[36]=b[26] then goto below5;
if b[36]=b[27] then goto below5;
if b[36]=b[28] then goto below5;
if b[36]=b[29] then goto below5;
if b[36]=b[30] then goto below5;
if b[36]=b[31] then goto below5;
if b[36]=b[33] then goto below5;
if b[36]=b[34] then goto below5;
if b[36]=b[35] then goto below5;

b[32]:=3*s-b[31]-b[33]-b[35]-b[36]-b[34];

if b[32]<1 then goto below5;
if b[32]>36 then goto below5;

if b[32]=b[1] then goto below5;
if b[32]=b[3] then goto below5;
if b[32]=b[4] then goto below5;
if b[32]=b[5] then goto below5;
if b[32]=b[7] then goto below5;
if b[32]=b[8] then goto below5;
if b[32]=b[9] then goto below5;
if b[32]=b[10] then goto below5;
if b[32]=b[11] then goto below5;
if b[32]=b[12] then goto below5;
if b[32]=b[13] then goto below5;
if b[32]=b[14] then goto below5;
if b[32]=b[15] then goto below5;
if b[32]=b[16] then goto below5;
if b[32]=b[17] then goto below5;
if b[32]=b[18] then goto below5;
if b[32]=b[19] then goto below5;
if b[32]=b[20] then goto below5;
if b[32]=b[21] then goto below5;
if b[32]=b[22] then goto below5;
if b[32]=b[23] then goto below5;
if b[32]=b[24] then goto below5;
if b[32]=b[25] then goto below5;
if b[32]=b[26] then goto below5;
if b[32]=b[27] then goto below5;
if b[32]=b[28] then goto below5;
if b[32]=b[29] then goto below5;
if b[32]=b[30] then goto below5;
if b[32]=b[31] then goto below5;
if b[32]=b[33] then goto below5;
if b[32]=b[34] then goto below5;
if b[32]=b[35] then goto below5;
if b[32]=b[36] then goto below5;

b[2]:=3*s-b[8]-b[14]-b[20]-b[26]-b[32];

if b[2]<1 then goto below5;
if b[2]>36 then goto below5;

if b[2]=b[1] then goto below5;
if b[2]=b[3] then goto below5;
if b[2]=b[4] then goto below5;
if b[2]=b[5] then goto below5;
if b[2]=b[7] then goto below5;
if b[2]=b[8] then goto below5;
if b[2]=b[9] then goto below5;
if b[2]=b[10] then goto below5;
if b[2]=b[11] then goto below5;
if b[2]=b[12] then goto below5;
if b[2]=b[13] then goto below5;
if b[2]=b[14] then goto below5;
if b[2]=b[15] then goto below5;
if b[2]=b[16] then goto below5;
if b[2]=b[17] then goto below5;
if b[2]=b[18] then goto below5;
if b[2]=b[19] then goto below5;
if b[2]=b[20] then goto below5;
if b[2]=b[21] then goto below5;
if b[2]=b[22] then goto below5;
if b[2]=b[23] then goto below5;
if b[2]=b[24] then goto below5;
if b[2]=b[25] then goto below5;
if b[2]=b[26] then goto below5;
if b[2]=b[27] then goto below5;
if b[2]=b[28] then goto below5;
if b[2]=b[29] then goto below5;
if b[2]=b[30] then goto below5;
if b[2]=b[31] then goto below5;
if b[2]=b[32] then goto below5;
if b[2]=b[33] then goto below5;
if b[2]=b[34] then goto below5;
if b[2]=b[35] then goto below5;
if b[2]=b[36] then goto below5;

b[6]:=3*s-b[1]-b[3]-b[5]-b[2]-b[4];

if b[6]<1 then goto below5;
if b[6]>36 then goto below5;

if b[6]=b[1] then goto below5;
if b[6]=b[2] then goto below5;
if b[6]=b[3] then goto below5;
if b[6]=b[4] then goto below5;
if b[6]=b[5] then goto below5;
if b[6]=b[7] then goto below5;
if b[6]=b[8] then goto below5;
if b[6]=b[9] then goto below5;
if b[6]=b[10] then goto below5;
if b[6]=b[11] then goto below5;
if b[6]=b[12] then goto below5;
if b[6]=b[13] then goto below5;
if b[6]=b[14] then goto below5;
if b[6]=b[15] then goto below5;
if b[6]=b[16] then goto below5;
if b[6]=b[17] then goto below5;
if b[6]=b[18] then goto below5;
if b[6]=b[19] then goto below5;
if b[6]=b[20] then goto below5;
if b[6]=b[21] then goto below5;
if b[6]=b[22] then goto below5;
if b[6]=b[24] then goto below5;
if b[6]=b[23] then goto below5;
if b[6]=b[25] then goto below5;
if b[6]=b[26] then goto below5;
if b[6]=b[27] then goto below5;
if b[6]=b[28] then goto below5;
if b[6]=b[29] then goto below5;
if b[6]=b[30] then goto below5;
if b[6]=b[31] then goto below5;
if b[6]=b[32] then goto below5;
if b[6]=b[33] then goto below5;
if b[6]=b[34] then goto below5;
if b[6]=b[35] then goto below5;
if b[6]=b[36] then goto below5;

writeln(aut,b[1],' ',b[2],' ',b[3],' ',b[4],' ',b[5],' ',b[6]);
writeln(aut,b[7],' ',b[8],' ',b[9],' ',b[10],' ',b[11],' ',b[12]);
writeln(aut,b[13],' ',b[14],' ',b[15],' ',b[16],' ',b[17],' ',b[18]);
writeln(aut,b[19],' ',b[20],' ',b[21],' ',b[22],' ',b[23],' ',b[24]);
writeln(aut,b[25],' ',b[26],' ',b[27],' ',b[28],' ',b[29],' ',b[30]);
writeln(aut,b[31],' ',b[32],' ',b[33],' ',b[34],' ',b[35],' ',b[36]);

below5:

end;
below4:

end;
below2:

end;
below1:

end;
below6:

end;
below8:

end;
below10:

end;end;

begin

  assign(aut,'data19.pas');
  rewrite(aut);

  s:=37;

   count:=0;

{these  3 ARE TOGETHER: i5 < i4 < i3}

  for i5:=1 to 1 do begin
  for i4:=17 to 17 do begin
  for i3:=22 to 22 do begin
                
b[21]:=i3;b[15]:=i5;b[16]:=i4;

if b[15]=b[21] then goto below9;

if b[16]=b[15] then goto below9;
if b[16]=b[21] then goto below9;

b[22]:=2*s-b[21]-b[16]-b[15];

if b[22]<1 then goto below9;
if b[22]>36 then goto below9;

if b[22]=b[15] then goto below9;
if b[22]=b[16] then goto below9;
if b[22]=b[21] then goto below9;

for i1:=7 to 7 do begin
for i2:=9 to 9 do begin {require m < o, i1 < i2}
for i15:=30 to 30 do begin
                
b[11]:=i1; b[26]:=i2; b[29]:=i15;

if b[11]=b[15] then goto below7;
if b[11]=b[16] then goto below7;
if b[11]=b[21] then goto below7;
if b[11]=b[22] then goto below7;

if b[26]=b[11] then goto below7;
if b[26]=b[15] then goto below7;
if b[26]=b[16] then goto below7;
if b[26]=b[21] then goto below7;
if b[26]=b[22] then goto below7;
                
if b[29]=b[11] then goto below7;
if b[29]=b[15] then goto below7;
if b[29]=b[16] then goto below7;
if b[29]=b[21] then goto below7;
if b[29]=b[22] then goto below7;
if b[29]=b[26] then goto below7;

b[8]:=2*s-b[11]-b[26]-b[29];

if b[8]<1 then goto below7;
if b[8]>36 then goto below7;

if b[8]=b[11] then goto below7;
if b[8]=b[15] then goto below7;
if b[8]=b[16] then goto below7;
if b[8]=b[21] then goto below7;
if b[8]=b[22] then goto below7;
if b[8]=b[26] then goto below7;
if b[8]=b[29] then goto below7;

for i9:=8 to 8 do begin
for i16:=29 to 29 do begin
  for i6:=10 to 10 do begin
    for i10:=15 to 15 do begin
     for i7:=36 to 36 do begin
      for i11:=20 to 20 do begin
       for i18:=3 to 3 do begin

b[9]:=i7; b[10]:=i11;b[14]:=i16; b[20]:=i9;
b[23]:=i6; b[27]:=i10; b[28]:=i18;

if b[20]=b[8] then goto below3;
if b[20]=b[11] then goto below3;
if b[20]=b[15] then goto below3;
if b[20]=b[16] then goto below3;
if b[20]=b[21] then goto below3;
if b[20]=b[22] then goto below3;
if b[20]=b[26] then goto below3;
if b[20]=b[29] then goto below3;

if b[9]=b[8] then goto below3;
if b[9]=b[11] then goto below3;
if b[9]=b[15] then goto below3;
if b[9]=b[16] then goto below3;
if b[9]=b[20] then goto below3;
if b[9]=b[21] then goto below3;
if b[9]=b[22] then goto below3;
if b[9]=b[26] then goto below3;
if b[9]=b[29] then goto below3;

if b[14]=b[8] then goto below3;
if b[14]=b[9] then goto below3;
if b[14]=b[11] then goto below3;
if b[14]=b[15] then goto below3;
if b[14]=b[16] then goto below3;
if b[14]=b[20] then goto below3;
if b[14]=b[21] then goto below3;
if b[14]=b[22] then goto below3;
if b[14]=b[26] then goto below3;
if b[14]=b[29] then goto below3;

if b[23]=b[8] then goto below3;
if b[23]=b[9] then goto below3;
if b[23]=b[11] then goto below3;
if b[23]=b[14] then goto below3;
if b[23]=b[15] then goto below3;
if b[23]=b[16] then goto below3;
if b[23]=b[20] then goto below3;
if b[23]=b[21] then goto below3;
if b[23]=b[22] then goto below3;
if b[23]=b[26] then goto below3;
if b[23]=b[29] then goto below3;

if b[27]=b[8] then goto below3;
if b[27]=b[9] then goto below3;
if b[27]=b[11] then goto below3;
if b[27]=b[14] then goto below3;
if b[27]=b[15] then goto below3;
if b[27]=b[16] then goto below3;
if b[27]=b[20] then goto below3;
if b[27]=b[21] then goto below3;
if b[27]=b[22] then goto below3;
if b[27]=b[23] then goto below3;
if b[27]=b[26] then goto below3;
if b[27]=b[29] then goto below3;

if b[10]=b[8] then goto below3;
if b[10]=b[9] then goto below3;
if b[10]=b[11] then goto below3;
if b[10]=b[14] then goto below3;
if b[10]=b[15] then goto below3;
if b[10]=b[16] then goto below3;
if b[10]=b[20] then goto below3;
if b[10]=b[21] then goto below3;
if b[10]=b[22] then goto below3;
if b[10]=b[23] then goto below3;
if b[10]=b[26] then goto below3;
if b[10]=b[27] then goto below3;
if b[10]=b[29] then goto below3;

if b[28]=b[8] then goto below3;
if b[28]=b[9] then goto below3;
if b[28]=b[10] then goto below3;
if b[28]=b[11] then goto below3;
if b[28]=b[14] then goto below3;
if b[28]=b[15] then goto below3;
if b[28]=b[16] then goto below3;
if b[28]=b[20] then goto below3;
if b[28]=b[21] then goto below3;
if b[28]=b[22] then goto below3;
if b[28]=b[23] then goto below3;
if b[28]=b[26] then goto below3;
if b[28]=b[27] then goto below3;
if b[28]=b[29] then goto below3;

 b[17]:=4*s-b[10]-b[9]-b[14]-b[20]-b[23]-b[27]-b[28];

if b[17]<1 then goto below3;
if b[17]>36 then goto below3;

if b[17]=b[8] then goto below3;
if b[17]=b[9] then goto below3;
if b[17]=b[10] then goto below3;
if b[17]=b[11] then goto below3;
if b[17]=b[14] then goto below3;
if b[17]=b[15] then goto below3;
if b[17]=b[16] then goto below3;
if b[17]=b[20] then goto below3;
if b[17]=b[21] then goto below3;
if b[17]=b[22] then goto below3;
if b[17]=b[23] then goto below3;
if b[17]=b[26] then goto below3;
if b[17]=b[27] then goto below3;
if b[17]=b[28] then goto below3;
if b[17]=b[29] then goto below3;

  count:=count+1; square;

  below3:

end;end;end;end;end;end;end;

below7:

end;end;end;

below9:

end;end;end;

close(aut);

 write(count); 

end.
