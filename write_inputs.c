#include <stdio.h>
#include <stdlib.h>

int main (int argc, char** argv)
{
  int u,v,z,o,m,e,p,r;
  if (argc != 4)
  {
    printf("Usage: ./write_inputs u v z\n");
    return 1;
  }

  char filename[80];
  FILE *outf;

  u = atoi(argv[1]);
  v = atoi(argv[2]);
  z = atoi(argv[3]);

  printf("Writing inputs for u=%d, v=%d, z=%d\n",u,v,z);

  for (o=1; o<=36; o++)
  {
    if (o==u) continue;
    if (o==v) continue;
    if (o==z) continue;
    // m < o
    for (m=1; m<o; m++)
    {
      if (m==u) continue;
      if (m==v) continue;
      if (m==z) continue;
      for (e=1; e<=36; e++)
      {
        if (e==m) continue;
        if (e==o) continue;
        if (e==u) continue;
        if (e==v) continue;
        if (e==z) continue;
        for (p=1; p<=36; p++)
        {
          if (p==e) continue;
          if (p==m) continue;
          if (p==o) continue;
          if (p==u) continue;
          if (p==v) continue;
          if (p==z) continue;
          for (r=1; r<=9; r++)
          {
            sprintf(filename,"u%d_v%d_z%d_o%d_m%d_e%d_p%d_r%d.inp",u,v,z,o,m,e,p,r);
            outf = fopen(filename, "w");
            fprintf(outf,"format compressed\n");
            fprintf(outf,"print 1000000\n");
            fprintf(outf,"u_min %d\n",u); 
            fprintf(outf,"u_max %d\n",u); 
            fprintf(outf,"v_min %d\n",v); 
            fprintf(outf,"v_max %d\n",v); 
            fprintf(outf,"z_min %d\n",z); 
            fprintf(outf,"z_max %d\n",z); 
            fprintf(outf,"o_min %d\n",o); 
            fprintf(outf,"o_max %d\n",o); 
            fprintf(outf,"m_min %d\n",m); 
            fprintf(outf,"m_max %d\n",m); 
            fprintf(outf,"e_min %d\n",e); 
            fprintf(outf,"e_max %d\n",e); 
            fprintf(outf,"p_min %d\n",p); 
            fprintf(outf,"p_max %d\n",p); 
            fprintf(outf,"r_min %d\n",(r-1)*4+1); 
            fprintf(outf,"r_max %d\n",r*4); 
            fclose(outf);
          }
        }
      }
    }
  }

  return 0;
}
