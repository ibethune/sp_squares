#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

int main(int argc, char* argv[])
{
   FILE * in=fopen(argv[1], "r");
 
   uint64_t s[36];
   uint64_t o[4];

   while (true)
   {
     if (fscanf(in,"%llu %llu %llu %llu\n", &o[0],&o[1],&o[2],&o[3])==EOF) break;

     s[0] = (o[0] >> 0 ) & 0x3f;
     s[1] = (o[0] >> 6 ) & 0x3f;
     s[2] = (o[0] >> 12 ) & 0x3f;
     s[3] = (o[0] >> 18 ) & 0x3f;
     s[4] = (o[0] >> 24 ) & 0x3f;
     s[5] = (o[0] >> 30 ) & 0x3f;
     s[6] = (o[0] >> 36 ) & 0x3f;
     s[7] = (o[0] >> 42 ) & 0x3f;
     s[8] = (o[0] >> 48 ) & 0x3f;

     s[9] = (o[1] >> 0 ) & 0x3f;
     s[10] = (o[1] >> 6 ) & 0x3f;
     s[11] = (o[1] >> 12 ) & 0x3f;
     s[12] = (o[1] >> 18 ) & 0x3f;
     s[13] = (o[1] >> 24 ) & 0x3f;
     s[14] = (o[1] >> 30 ) & 0x3f;
     s[15] = (o[1] >> 36 ) & 0x3f;
     s[16] = (o[1] >> 42 ) & 0x3f;
     s[17] = (o[1] >> 48 ) & 0x3f;

     s[18] = (o[2] >> 0 ) & 0x3f;
     s[19] = (o[2] >> 6 ) & 0x3f;
     s[20] = (o[2] >> 12 ) & 0x3f;
     s[21] = (o[2] >> 18 ) & 0x3f;
     s[22] = (o[2] >> 24 ) & 0x3f;
     s[23] = (o[2] >> 30 ) & 0x3f;
     s[24] = (o[2] >> 36 ) & 0x3f;
     s[25] = (o[2] >> 42 ) & 0x3f;
     s[26] = (o[2] >> 48 ) & 0x3f;

     s[27] = (o[3] >> 0 ) & 0x3f;
     s[28] = (o[3] >> 6 ) & 0x3f;
     s[29] = (o[3] >> 12 ) & 0x3f;
     s[30] = (o[3] >> 18 ) & 0x3f;
     s[31] = (o[3] >> 24 ) & 0x3f;
     s[32] = (o[3] >> 30 ) & 0x3f;
     s[33] = (o[3] >> 36 ) & 0x3f;
     s[34] = (o[3] >> 42 ) & 0x3f;
     s[35] = (o[3] >> 48 ) & 0x3f;

     printf("%llu %llu %llu %llu %llu %llu\n", s[0],s[1],s[2],s[3],s[4],s[5]);
     printf("%llu %llu %llu %llu %llu %llu\n", s[6],s[7],s[8],s[9],s[10],s[11]);
     printf("%llu %llu %llu %llu %llu %llu\n", s[12],s[13],s[14],s[15],s[16],s[17]);
     printf("%llu %llu %llu %llu %llu %llu\n", s[18],s[19],s[20],s[21],s[22],s[23]);
     printf("%llu %llu %llu %llu %llu %llu\n", s[24],s[25],s[26],s[27],s[28],s[29]);
     printf("%llu %llu %llu %llu %llu %llu\n", s[30],s[31],s[32],s[33],s[34],s[35]);

   }

   fclose(in);
   return 0;

}
