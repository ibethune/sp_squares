#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

int main(int argc, char* argv[])
{
   FILE * in=fopen(argv[1], "r");
 
   uint64_t s[36];
   uint64_t o[4];

   while (true)
   {
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[0],&s[1],&s[2],&s[3],&s[4],&s[5])==EOF) break;
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[6],&s[7],&s[8],&s[9],&s[10],&s[11])==EOF) break;
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[12],&s[13],&s[14],&s[15],&s[16],&s[17])==EOF) break;
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[18],&s[19],&s[20],&s[21],&s[22],&s[23])==EOF) break;
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[24],&s[25],&s[26],&s[27],&s[28],&s[29])==EOF) break;
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[30],&s[31],&s[32],&s[33],&s[34],&s[35])==EOF) break;

     o[0] = ((s[0] & 0x3f) << 0) | ((s[1] & 0x3f) << 6) |
            ((s[2] & 0x3f) << 12) | ((s[3] & 0x3f) << 18) |
            ((s[4] & 0x3f) << 24) | ((s[5] & 0x3f) << 30) | 
            ((s[6] & 0x3f) << 36) | ((s[7] & 0x3f) << 42) | 
            ((s[8] & 0x3f) << 48);
     
     o[1] = ((s[9] & 0x3f) << 0) | ((s[10] & 0x3f) << 6) |
            ((s[11] & 0x3f) << 12) | ((s[12] & 0x3f) << 18) |
            ((s[13] & 0x3f) << 24) | ((s[14] & 0x3f) << 30) |
            ((s[15] & 0x3f) << 36) | ((s[16] & 0x3f) << 42) |
            ((s[17] & 0x3f) << 48);

     o[2] = ((s[18] & 0x3f) << 0) | ((s[19] & 0x3f) << 6) |
            ((s[20] & 0x3f) << 12) | ((s[21] & 0x3f) << 18) |
            ((s[22] & 0x3f) << 24) | ((s[23] & 0x3f) << 30) |
            ((s[24] & 0x3f) << 36) | ((s[25] & 0x3f) << 42) |
            ((s[26] & 0x3f) << 48);

     o[3] = ((s[27] & 0x3f) << 0) | ((s[28] & 0x3f) << 6) |
            ((s[29] & 0x3f) << 12) | ((s[30] & 0x3f) << 18) |
            ((s[31] & 0x3f) << 24) | ((s[32] & 0x3f) << 30) |
            ((s[33] & 0x3f) << 36) | ((s[34] & 0x3f) << 42) |
            ((s[35] & 0x3f) << 48);
     printf("%llu %llu %llu %llu\n",o[0], o[1], o[2], o[3]);

   }

   fclose(in);
   return 0;

}
