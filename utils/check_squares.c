#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

int main(int argc, char* argv[])
{
   FILE * in=fopen(argv[1], "r");
 
   uint64_t s[36];
   bool ok;

   while (true)
   {
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[0],&s[1],&s[2],&s[3],&s[4],&s[5])==EOF) break;
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[6],&s[7],&s[8],&s[9],&s[10],&s[11])==EOF) break;
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[12],&s[13],&s[14],&s[15],&s[16],&s[17])==EOF) break;
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[18],&s[19],&s[20],&s[21],&s[22],&s[23])==EOF) break;
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[24],&s[25],&s[26],&s[27],&s[28],&s[29])==EOF) break;
     if (fscanf(in,"%llu %llu %llu %llu %llu %llu\n", &s[30],&s[31],&s[32],&s[33],&s[34],&s[35])==EOF) break;

     // check rows
     ok =       (s[0] + s[1] + s[2] + s[3] + s[4] + s[5] == 111);
     ok = ok && (s[6] + s[7] + s[8] + s[9] + s[10] + s[11] == 111);
     ok = ok && (s[12] + s[13] + s[14] + s[15] + s[16] + s[17] == 111);
     ok = ok && (s[18] + s[19] + s[20] + s[21] + s[22] + s[23] == 111);
     ok = ok && (s[24] + s[25] + s[26] + s[27] + s[28] + s[29] == 111);
     ok = ok && (s[30] + s[31] + s[32] + s[33] + s[34] + s[35] == 111);

     // check cols
     ok = ok && (s[0] + s[6] + s[12] + s[18] + s[24] + s[30] == 111);
     ok = ok && (s[1] + s[7] + s[13] + s[19] + s[25] + s[31] == 111);
     ok = ok && (s[2] + s[8] + s[14] + s[20] + s[26] + s[32] == 111);
     ok = ok && (s[3] + s[9] + s[15] + s[21] + s[27] + s[33] == 111);
     ok = ok && (s[4] + s[10] + s[16] + s[22] + s[28] + s[34] == 111);
     ok = ok && (s[5] + s[11] + s[17] + s[23] + s[29] + s[35] == 111);

     // check diagonals

     ok = ok && (s[0] + s[7] + s[14] + s[21] + s[28] + s[35] == 111);
     ok = ok && (s[5] + s[10] + s[15] + s[20] + s[25] + s[30] == 111);

     // check the two broken diagonals (for an SP square)

     ok = ok && (s[3] + s[10] + s[17] + s[18] + s[25] + s[32] == 111);
     ok = ok && (s[2] + s[7] + s[12] + s[23] + s[28] + s[33] == 111);

     // check every entry appears once

     int i, j;
     for (i = 1; i <= 36; i++)
     {
       for (j = 0; j < 36; j++)
       {
         if (s[j] == i) break;
       }
       if (j==36)
       {
         ok = false;
         break;
       }
     }

     if (!ok)
     {
       printf("Square failed!!!!\n");
       printf("%llu %llu %llu %llu %llu %llu\n", s[0],s[1],s[2],s[3],s[4],s[5]);
       printf("%llu %llu %llu %llu %llu %llu\n", s[6],s[7],s[8],s[9],s[10],s[11]);
       printf("%llu %llu %llu %llu %llu %llu\n", s[12],s[12],s[13],s[14],s[15],s[16]);
       printf("%llu %llu %llu %llu %llu %llu\n", s[18],s[19],s[20],s[21],s[22],s[23]);
       printf("%llu %llu %llu %llu %llu %llu\n", s[24],s[25],s[26],s[27],s[28],s[29]);
       printf("%llu %llu %llu %llu %llu %llu\n", s[30],s[31],s[32],s[33],s[34],s[35]);
     }
   }

   fclose(in);
   return 0;
}
